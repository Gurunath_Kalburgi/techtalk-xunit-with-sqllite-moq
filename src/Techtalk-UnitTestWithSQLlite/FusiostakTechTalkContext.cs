﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Techtalk_UnitTestWithSQLlite
{
    public class FusiostakTechTalkContext : DbContext
    {
        private readonly string _connectionString;

        //ctor + tab
        public FusiostakTechTalkContext(string connectionString) 
        {
            _connectionString = connectionString;
        }

        public FusiostakTechTalkContext(DbContextOptions<FusiostakTechTalkContext> options): base(options)
        {

        }  

        public DbSet<Members> Members { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder) 
        {
            if (!dbContextOptionsBuilder.IsConfigured) { 
            dbContextOptionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
