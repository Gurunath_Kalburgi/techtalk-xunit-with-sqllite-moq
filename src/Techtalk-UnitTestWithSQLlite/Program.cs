﻿using System;
using System.Linq;

namespace Techtalk_UnitTestWithSQLlite
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var _dbContext = new FusiostakTechTalkContext(@"Persist Security Info = False; Integrated Security = true; Initial Catalog = Test; server = GK-THINKPAD\SQLEXPRESS");

            var memeber = _dbContext.Members.FirstOrDefault(prop => prop.Id == 1);
            Console.WriteLine($"Name: {memeber.FirstName} {memeber.LastName}, Address: {memeber.Address}");

        }
    }
}
