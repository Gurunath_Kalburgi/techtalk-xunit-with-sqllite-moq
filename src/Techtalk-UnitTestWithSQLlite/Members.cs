﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Techtalk_UnitTestWithSQLlite
{
    [Table("Members")]
    public class Members
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string  LastName { get; set; }
        public string  Address { get; set; }
    }
}