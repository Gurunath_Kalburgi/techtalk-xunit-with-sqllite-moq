﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Techtalk_UnitTestWithSQLlite;
using Xunit;
using System.IO;
namespace TechTalkUnitWithSqlLite.Test
{
    public class MemberProviderTest
    {
        [Fact]
        public void Test_Get_By_Id()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<FusiostakTechTalkContext>().UseSqlite(connection).Options;

            using (var _dbContext = new FusiostakTechTalkContext(options))
            {
                _dbContext.Database.EnsureCreated();
            }

            using (var _dbContext = new FusiostakTechTalkContext(options))
            {
                _dbContext.Members.Add(new Members { Id = 1, FirstName = "John", LastName = "Cena", Address = "Us" });
                _dbContext.SaveChanges();
            }

            using (var _dbContext = new FusiostakTechTalkContext(options))
            {
                var member = _dbContext.Members.FirstOrDefault(prop => prop.Id == 1);

                Assert.Equal("John", member.FirstName);
            }
        }

        [Fact]
        public void Test_True()
        {
            //Arrange

            //act

            //Assert
            Assert.True(true);

               
        }

        [Fact]
        public void File_NotFound_Exception() 
        {
            Assert.Throws<FileNotFoundException>(() => File.ReadAllText(@"D:\Projects\playground-programming\fs-techtalk\test.txt"));
        }

        [Fact]
        public void File_Found()
        {
            Assert.False(false);
        }
    }
}
